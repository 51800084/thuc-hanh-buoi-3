import 'package:flutter/material.dart';
import '../model/image_model.dart';
import '../stream/image_stream.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  // This widget is the root of application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Image Stream Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Image Stream Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GetImageStream stream = GetImageStream();
  List<ImageModel> imgList = [];

  _MyHomePageState() {
    getStreamContent();
  }

  getStreamContent() async {
    stream.getImage().listen((imageObj) => setState(() {
      imgList.add(imageObj!);
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView.builder(
            itemCount: imgList.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                padding: const EdgeInsets.all(8),
                // Prevent null value by using replace value
                child: Image.network(imgList[index].url ?? ""),
              );
            }));
  }
}



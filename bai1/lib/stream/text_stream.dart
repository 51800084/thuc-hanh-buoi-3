import 'package:flutter/material.dart';

class TextStream {
  // Stream textStream;

  // Stream<Color> getColors() async* {
  //   final List<Color> colors = [
  //     Colors.blueGrey,
  //     Colors.amber,
  //     Colors.lightBlue,
  //     Colors.lightGreen,
  //     Colors.black26
  //   ];
  Stream<String> getText() async* {
    final List<String> texts = [
      'Eula',
      'Hutao',
      'Ei'
    ];
    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t%3;
      return texts[index];
    });
  }

  //   yield* Stream.periodic(Duration(seconds: 1), (int t) {
  //     int index = t % 5;
  //     return colors[index];
  //   });
  // }
}
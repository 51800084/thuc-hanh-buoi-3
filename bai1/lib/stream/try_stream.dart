import 'dart:async';

class Bloc {

  final demoController = StreamController<String>();
  // Retrieve dat
  receiveData(String msg) {
    demoController.sink.add(msg);
  }
  // Listen
  Bloc() {
    // Capture data and processing...
    demoController.stream.listen(
            (message) {
          print(message);
        }
    );
  }
}
void main() {
  Bloc a = Bloc();

  a.receiveData('Hello');
  a.receiveData('Stream');

  // a.receiveData('....');
}